from django.contrib import admin
from django.urls import path, re_path, include
from djoser import views as djoser_views
from rest_framework_jwt import views as jwt_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('spauser.urls'))
]
