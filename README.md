<h1>Hidden Founders web coding challenge - Baackend</h1>

A challenge proposed by Hidden Founders (https://github.com/hiddenfounders/web-coding-challenge/blob/master/coding-challenge.md)

<hr />

<h3>Functional spec</h3>
<ul>
    <li>As a User, I can sign up using my email & password [DONE] </li>
    <li>Sign in using email and password [DONE]</li>
    <li>Display list of nearby shops [DONE]</li>
    <li>Like a shop and display it in my preferred page [DONE]</li>
    <li>Dislike a shop and remove it from the nearby shops page for 2 hours [UNDONE]</li>
    <li>Unlike a shop and remove it from my preferred shops [DONE] </li>
</ul>
<ul>
    Bonus
    <li>[BONUS] As a User, I can display the list of preferred shops [DONE]</li>
    <li>[BONUS] As a User, I can remove a shop from my preferred shops list [DONE]</li>
    <li>[BONUS] As a User, I can dislike a shop, so it won’t be displayed within “Nearby Shops” list during the next 2 hours [UNDONE]</li>
</ul>

<h3><a href="https://gitlab.com/slimaidar/challenge-frontend">Frontend</a></h3>